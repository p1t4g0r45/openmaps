package openmaps.controllers;

import openmaps.persistence.PrivateNote;
import openmaps.persistence.PrivateNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("/")
    String index() {
        return "index";
    }
}