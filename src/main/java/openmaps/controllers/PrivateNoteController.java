package openmaps.controllers;

import openmaps.persistence.PrivateNote;
import openmaps.persistence.PrivateNoteRepository;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by piotrek on 25.06.2016.
 */

@Controller
public class PrivateNoteController {

    @Autowired
    private PrivateNoteRepository privateNoteRepository;

    @RequestMapping(value = "/add-note", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void addNote(Model model,
                       @RequestParam("lat") Double lat,
                       @RequestParam("lon") Double lon,
                       @RequestParam("text") String text) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if(userName == null || userName.equals("anonymousUser"))
            return;
        PrivateNote privateNote = new PrivateNote();
        privateNote.setLat(lat);
        privateNote.setLon(lon);
        privateNote.setText(text);
        privateNote.setUsername(userName);
        privateNoteRepository.save(privateNote);
    }

    @RequestMapping(value = "/get-notes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Collection<PrivateNote> getNotes(Model model) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if(userName == null || userName.equals("anonymousUser"))
            return null;
        Collection<PrivateNote> toReturn  = new ArrayList<>();
        for(PrivateNote object : privateNoteRepository.findAll()){
            if(userName.equals(object.getUsername()))
                toReturn.add(object);
        }
        return toReturn;
    }

}
