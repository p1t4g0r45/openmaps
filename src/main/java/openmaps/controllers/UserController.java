package openmaps.controllers;

import openmaps.pojo.UserDetails;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Created by baran on 02.05.16.
 */
@Controller
public class UserController {

    private static <T> T request(String url, Class<T> clazz) throws HttpClientErrorException {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        String password = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
        String plainCreds = activeUsername + ":" + password;
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, request, clazz);
        return response.getBody();
    }

    public static void nonAuthenticatedGetRequest() {
        RestTemplate restTemplate = new RestTemplate();
        UserDetails details = restTemplate.getForObject("http://api.openstreetmap.org/api/0.6/user/3900813", UserDetails.class);
        System.out.println(details.getUser().getId());
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String getDetails(Model model) {
        try {
            UserDetails details = request("http://api.openstreetmap.org/api/0.6/user/details", UserDetails.class);
            model.addAttribute("user", details.getUser());
            model.addAttribute("error", false);
        } catch (HttpClientErrorException e) {
            model.addAttribute("error", true);
            return "redirect:login";
        }
        return "details";
    }
}