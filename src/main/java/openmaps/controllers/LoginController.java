package openmaps.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by baran on 01.05.16.
 */
@Controller
public class LoginController {

    @RequestMapping("/login")
    String loginPage() {
        return "login";
    }

}