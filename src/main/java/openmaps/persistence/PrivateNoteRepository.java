package openmaps.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by piotrek on 24.06.2016.
 */

public interface PrivateNoteRepository extends CrudRepository<PrivateNote, Integer> {

}
