package openmaps.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement(name = "osm")
public class UserDetails {
    @XmlElement
    private User user;

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "ClassPojo [user = " + user + "]";
    }
}
