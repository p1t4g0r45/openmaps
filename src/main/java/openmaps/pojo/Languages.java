package openmaps.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class Languages {

    @XmlElement
    private String[] lang;

    public String[] getLang() {
        return lang;
    }

    @Override
    public String toString() {
        return "ClassPojo [lang = " + lang + "]";
    }
}