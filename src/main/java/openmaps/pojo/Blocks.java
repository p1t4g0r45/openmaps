package openmaps.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class Blocks {
    @XmlElement
    private Received received;

    public Received getReceived() {
        return received;
    }

    @Override
    public String toString() {
        return "ClassPojo [received = " + received + "]";
    }
}
