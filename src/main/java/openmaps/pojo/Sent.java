package openmaps.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class Sent {
    @XmlAttribute
    private String count;

    public String getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "ClassPojo [count = " + count + "]";
    }
}
