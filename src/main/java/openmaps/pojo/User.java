package openmaps.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class User {
    @XmlAttribute
    private String id;

    @XmlAttribute(name = "display_name")
    private String displayName;

    @XmlAttribute(name = "account_created")
    private String accountCreated;

    @XmlElement
    private Languages languages;

    @XmlElement(name = "contributor-terms")
    private ContributorTerms contributorTerms;

    @XmlElement
    private String description;

    @XmlElement
    private String roles;

    @XmlElement
    private Traces traces;

    @XmlElement
    private Img img;

    @XmlElement
    private Changesets changesets;

    @XmlElement
    private Blocks blocks;

    @XmlElement
    private Messages messages;

    public String getId() {
        return id;
    }

    public String getDisplay_name() {
        return displayName;
    }

    public String getAccount_created() {
        return accountCreated;
    }

    public Languages getLanguages() {
        return languages;
    }

    public ContributorTerms getContributorTerms() {
        return contributorTerms;
    }

    public String getDescription() {
        return description;
    }

    public String getRoles() {
        return roles;
    }

    public Traces getTraces() {
        return traces;
    }

    public Img getImg() {
        return img;
    }

    public Changesets getChangesets() {
        return changesets;
    }

    public Blocks getBlocks() {
        return blocks;
    }

    public Messages getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", display_name = " + displayName + ", account_created = " + accountCreated + ", languages = " + languages + ", contributor-terms = " + contributorTerms + ", description = " + description + ", roles = " + roles + ", traces = " + traces + ", img = " + img + ", changesets = " + changesets + ", blocks = " + blocks + ", messages = " + messages + "]";
    }
}