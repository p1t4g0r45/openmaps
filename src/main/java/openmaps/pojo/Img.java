package openmaps.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class Img {
    @XmlAttribute
    private String href;

    public String getHref() {
        return href;
    }

    @Override
    public String toString() {
        return "ClassPojo [href = " + href + "]";
    }
}
