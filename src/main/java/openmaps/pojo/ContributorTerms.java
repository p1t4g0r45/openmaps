package openmaps.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement(name = "contributor-terms")
public class ContributorTerms {

    @XmlAttribute
    private String pd;

    @XmlAttribute
    private String agreed;

    public String getPd() {
        return pd;
    }

    public String getAgreed() {
        return agreed;
    }

    @Override
    public String toString() {
        return "ClassPojo [pd = " + pd + ", agreed = " + agreed + "]";
    }
}