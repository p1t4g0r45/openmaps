package openmaps.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class Messages {
    @XmlElement
    private Sent sent;

    @XmlElement
    private Received received;

    public Sent getSent() {
        return sent;
    }

    public Received getReceived() {
        return received;
    }

    @Override
    public String toString() {
        return "ClassPojo [sent = " + sent + ", received = " + received + "]";
    }
}