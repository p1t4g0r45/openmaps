package openmaps.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baran on 30.04.16.
 */
@XmlRootElement
public class Received {
    @XmlAttribute
    private String count;

    @XmlAttribute
    private String active;

    public String getCount() {
        return count;
    }

    public String getActive() {
        return active;
    }

    @Override
    public String toString() {
        return "ClassPojo [count = " + count + ", active = " + active + "]";
    }
}