var mymap = L.map('mapid').setView([50.068, 19.912], 14);
var feature;

var notes = [];
var notesGroup;
var notesVisible = true;

var privateNotes = [];
var privateNotesVisible = true;

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

var greenIcon = new L.Icon({
  iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

var blueIcon = new L.Icon({
  iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

function httpGet(lat, lon, zoom, popup) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            popup.setContent(xmlHttp.responseXML.getElementsByTagName("result").item(0).textContent);
        }
    }
    var format = "xml";
    var addressdetails = "1";
    var params = "lat=" + lat + "&lon=" + lon + "&format=" + format + "&zoom=" + zoom + "&addressdetails=" + addressdetails;
    xmlHttp.open("POST", "http://nominatim.openstreetmap.org/reverse?" + params, true);
    xmlHttp.send(null);
}

var popup = L.popup();

function onMapClick(e) {
    popup
            .setLatLng(e.latlng)
            .setContent("")
            .openOn(mymap);
    httpGet(e.latlng.lat, e.latlng.lng, mymap.getZoom(), popup);
}

mymap.on('click', onMapClick);


function onMapRightClick(e) {
    popup
            .setLatLng(e.latlng)
            .setContent("<input type=\"text\"/ id=\"private_note_input\">")
            .openOn(mymap);

    $('#private_note_input').keypress(function (e1) {
      if (e1.which == 13) {
        mymap.closePopup();
        marker = new L.marker(e.latlng, {icon: blueIcon}).bindPopup(this.value);
        privateNotes.push(marker);
        mymap.addLayer(marker);

        //send note to server
        var csrfToken = $("meta[name='_csrf']").attr("content");
        var csrfParameter = $("meta[name='_csrf_name']").attr("content");
        $.post("add-note",
        {
            lat: e.latlng.lat,
            lon: e.latlng.lng,
            text: this.value,
            _csrf: csrfToken
        },
        function(data, status){});
      }
    });
}

mymap.on('contextmenu', onMapRightClick);


function chooseAddr(lat1, lng1, lat2, lng2, osm_type, display_name) {
    $("input#search_input").val(display_name);

    var loc1 = new L.LatLng(lat1, lng1);
    var loc2 = new L.LatLng(lat2, lng2);
    var bounds = new L.LatLngBounds(loc1, loc2);

    if (feature) {
        mymap.removeLayer(feature);
    }
    if (osm_type == "node") {
        feature = L.circle(loc1, 25, {color: 'green', fill: false}).addTo(mymap);
        mymap.fitBounds(bounds);
        mymap.setZoom(18);
    } else {
        var loc3 = new L.LatLng(lat1, lng2);
        var loc4 = new L.LatLng(lat2, lng1);

        feature = L.polyline([loc1, loc4, loc2, loc3, loc1], {color: 'red'}).addTo(mymap);
        mymap.fitBounds(bounds);
    }
}

function drawNotes() {
    bounds = mymap.getBounds();
    l = bounds.getWest();
    b = bounds.getSouth();
    r = bounds.getEast();
    t = bounds.getNorth();

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            $.each(notes, function (key, note) {
                mymap.removeLayer(note);
            });
            if(!notesVisible)
                return;
            notes = [];
            $.each(xmlHttp.responseXML.getElementsByTagName("note"), function (key, note) {
                marker = new L.marker([note.getAttribute("lat"), note.getAttribute("lon")], {icon: greenIcon})
                    .bindPopup(note.getElementsByTagName("text")[0].textContent);
                notes.push(marker);
                mymap.addLayer(marker);
            });
        }
    }
    xmlHttp.open("GET", "http://api.openstreetmap.org/api/0.6/notes?bbox=" + l + "," + b + "," + r + "," + t, true);
    xmlHttp.send(null);
}

drawNotes();
mymap.on('moveend', drawNotes);

$(document).ready(function () {

    $("#checkbox1").change(function() {
        notesVisible = this.checked;
        drawNotes();
    });

     $("#checkbox2").change(function() {
         privateNotesVisible = this.checked;
         $.each(privateNotes, function (key, note) {
             mymap.removeLayer(note);
         });
         if(privateNotesVisible){
             $.each(privateNotes, function (key, note) {
                mymap.addLayer(note);
             });
         }
     });

    var searchResults = $('#search_results ul');
    $('#search_input').bind("keyup change input", function () {

        if (feature) {
            mymap.removeLayer(feature);
        }

        var typedValue = $('#search_input').val();
        if (typedValue.length < 2) {
            searchResults.empty();
            $('#search_results').empty();
            return;
        }
        clearTimeout($.data(this, 'timer'));
        $(this).data('timer', setTimeout(search(typedValue), 100));
    });

    function search(typedValue) {

        bounds = mymap.getBounds();
        l = bounds.getWest();
        b = bounds.getSouth();
        r = bounds.getEast();
        t = bounds.getNorth();

        $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=5&bounded=1&viewboxlbrt=' + l + ',' + b + ',' + r + ',' + t + '&q=' + typedValue, function (data) {
            var items = [];

            $.each(data, function (key, val) {
                bb = val.boundingbox;
                items.push("<li><a href='#' class='result-a' onclick='chooseAddr(" + bb[0] + ", " + bb[2] + ", " + bb[1] + ", " + bb[3] + ", \"" + val.osm_type + "\", \"" + val.display_name + "\");return false;'>" + val.display_name + '</a></li>');
            });

            $('#search_results').empty();
            if (items.length != 0) {
                $('<ul/>', {
                    'class': 'my-new-list',
                    html: items.join('')
                }).appendTo('#search_results');
            }
            setWidth();
        });
    }

    $.getJSON('get-notes', function (data) {
        $.each(data, function (key, val) {
            marker = new L.marker([val.lat, val.lon], {icon: blueIcon}).bindPopup(val.text);
            privateNotes.push(marker);
            mymap.addLayer(marker);
        });
    });

});

$("input#search_input").focus(function () {
    $("span#search_results").css("visibility", "initial");
});
$("input#search_input").blur(function () {
    clearTimeout($.data(this, 'timerBlur'));
    $(this).data('timerBlur', setTimeout(function () {
        $("span#search_results").css("visibility", "hidden");
    }, 150));
});

function setWidth() {
    $(".result-a").width($("input#search_input").width() - 62);
}

window.onresize = function () {
    setWidth();
}