# README #

This is a mini-project showing how to use OpenStreetMaps

### Project setup (IntelliJ) ###

* New -> Project from Version Control -> Git
* enter https://<username>@bitbucket.org/p1t4g0r45/openmaps.git
* authenticate
* make project with gradle

### MySQL setup ###

```
#!bash

$ sudo mysql
mysql> create database maps;
mysql> create user 'mapuser'@'localhost' identified by 'password123'
mysql> grant all privileges on * . * to 'mapuser'@'localhost';
```

### Sample OpenStreetMap account ###
* username: AGH_super_uczelnia
* password: password123

### Implementation details (Technologies used) ###
* Gradle build tool
* Java backend
* HTML, CSS, JS frontend
* Spring (Initializr, Boot, MVC, Security, Data)
* OpenStreetMap REST Api
* OAuth authorization #not implemented yet
* Leaflet - JS library for interactive maps
* MySQL databse #not used yet